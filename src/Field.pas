unit field;
{
------------------------------------------------------------------
WATOMIC - Windows(c) KAtomic clone

          PlayField and atoms implementation unit;

Copyright (C) 2004-2005  Federico Blaseotto (ironfede)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
------------------------------------------------------------------
}

//GLTexture: line 3996 produces an error on CyberTrident;
//there is a problem somewhere in OpenGL drivers

interface

uses Classes, GLObjects, GLScene, Types, GLCadencer, IniFiles, Graphics;

const
  CELL_LENGTH = 1/16;
  V_CELL_LENGTH = CELL_LENGTH;

//***************************************************************************
//Some helpers in creating atoms
var
  temp_atom,temp:TBitmap;
  Binds_graph:TBitmap;
  r:TRect;

//***************************************************************************
//Represents atom movement type
type
  TMovement = (mUp,mLeft,mRight,mDown);

//***************************************************************************
//Destination of moving atom in the text grid
  TDestination = record
    row:integer;
    col:integer;
    end;

//***************************************************************************
//Textual field representation
  TPlayGrid = array [0..14,0..14] of char;

//***************************************************************************
//Atom on glscene class; it implements graphic and movement behaviour.
  TWAtom = class(TGLSprite)
    private
      FSpec: string;     //specie chimica e legame
      FAtomID: string;   //Atom's ID
      FRow: integer;
      FCol: integer;
      FMovType: TMovement;
      FDestination: TDestination;
      FIsMoving: boolean;
      FNextAtom: TWAtom;

      procedure SetSpec(const Value: string);
      procedure SetAtomID(const Value: string);
      procedure SetCol(const Value: integer);
      procedure SetRow(const Value: integer);
      procedure SetMovType(const Value: TMovement);
      procedure SetIsMoving(const Value: boolean);
      procedure SetNextAtom(const Value: TWAtom);

      property Spec:string read FSpec write SetSpec;
      property AtomID:string read FAtomId write SetAtomID;
      property Col:integer  read FCol write SetCol;
      property Row:integer  read FRow write SetRow;
      property MovType:TMovement read FMovType write SetMovType;

    public
      constructor Create(AOwner:TComponent);override;
      destructor Destroy;override;

      property IsMoving:boolean read FIsMoving write SetIsMoving;
      property NextAtom:TWAtom read FNextAtom write SetNextAtom;

    end;

//***************************************************************************
  TPlayField = class(TGLScene)
    private
      PlayGrid:TPlayGrid;
      LevelFile: TMemIniFile;

      FLevel: string;
      FSelected: TWatom;
      FMolek_Name: string;
      FMoves: integer;
      FOnLevelCompleted: TNotifyEvent;
      FLevelNumber: integer;
      FWallBitmap:TBitmap;
      FSpeed: integer;

      procedure SetLevel(const Value: string);
      procedure SetSelected(const Value: TWatom);
      procedure SetMolek_Name(const Value: string);
      procedure SetMoves(const Value: integer);
      procedure SetOnLevelCompleted(const Value: TNotifyEvent);

    protected
      procedure DoLevelCompleted;dynamic;
      procedure DoCadProgress(Sender: TObject; const deltaTime,newTime: Double);dynamic;

    public
      GoalViewer:TGLScene;
      Cd:TGLCadencer;

      constructor Create(AOwner:TComponent);override;
      destructor Destroy;override;

      property Molek_Name:string read FMolek_Name write SetMolek_Name;
      property Level:string read FLevel write SetLevel;
      property Selected:TWatom read FSelected write SetSelected;
      property Moves:integer read FMoves write SetMoves;
      property LevelNumber:integer read FLevelNumber;
      property OnLevelCompleted:TNotifyEvent read FOnLevelCompleted write SetOnLevelCompleted;
      property Speed:integer read FSpeed write FSpeed;

      function IsCompleted:boolean;
      procedure MoveAtomLeft;
      procedure MoveAtomRight;
      procedure MoveAtomUp;
      procedure MoveAtomDown;

      procedure SaveLevel(AFileName:string);
      procedure LoadLevel(AFileName:string);

      procedure ApplyMover;
      procedure RemoveMover;
      procedure ClearField;

    end;

//------------------------------------------------------------------------------
//----------------------------IMPLEMENTATION------------------------------------
//------------------------------------------------------------------------------

implementation

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//images.res --HAS-- to be compiled
//before building WAtomic;
//it contains all atoms and binds images
//you can use the "compile_res.bat" in 'src' dir.
{$R images.res}

uses GLUtils,
     GlTexture,
     SysUtils,
     Dialogs;

//------------------------------------------------------------------------------
{ TPlayField }

constructor TPlayField.Create(AOwner: TComponent);
begin
  inherited;
  Cd:=TGLCadencer.Create(Self);
  Cd.Enabled:=FALSE;
  Cd.Scene:=Self;
  //Cd.TimeMultiplier:=-1;
  Cd.OnProgress:=DoCadProgress;

  FWallBitmap:=TBitmap.Create;
  FWallBitmap.Height:=32;
  FWallBitmap.Transparent:=FALSE;
  FWallBitmap.Width:=32;
  FWallBitmap.Assign(nil);
  FWallBitmap.LoadFromResourceName(hinstance,'WALL');

end;

//------------------------------------------------------------------------------
destructor TPlayField.Destroy;
begin
  FreeAndNil(LevelFile);
  FreeAndNil(Cd);
  FreeAndNil(FWallBitmap);

  inherited;
end;

//------------------------------------------------------------------------------
//Here the atom's movements are implemented through cadencer
procedure TPlayField.DoCadProgress(Sender: TObject; const deltaTime,
  newTime: Double);
var
  STEP:Double;

const
  Speed1X = 0.08;
  Speed10X = 1.9;

begin
  STEP := (Speed1X + (FSpeed - 1) * (Speed10X - Speed1X) / 9) * deltaTime;

  //The following stuff is organized in order to avoid
  //positioning errors due approximation in floating point
  //operations.

  Case Selected.MovType of

  mLeft:
    if  STEP<-(-7*CELL_LENGTH+CELL_LENGTH*Selected.FDestination.col) + Selected.Position.X then
        begin
        //posX:= posX-0.1;
        Selected.Position.X := Selected.Position.X - STEP;
        exit;
        //ot:=Cad.GetCurrentTime;  Cad.CurrentTime-ot
        end;

  mRight:
      if (STEP) < (-7*CELL_LENGTH+CELL_LENGTH*Selected.FDestination.col) - Selected.Position.X then

        begin
        Selected.Position.X :=Selected.Position.X + STEP;
        exit;
        end;

  mUp:
      if -Selected.Position.Y + (7*CELL_LENGTH - CELL_LENGTH*Selected.FDestination.row) > STEP then
        begin
        Selected.Position.Y :=Selected.Position.Y +STEP;;
        exit;
        end;

  mDown:
      if (Selected.Position.Y -(7*CELL_LENGTH - CELL_LENGTH*Selected.FDestination.row) > STEP ) then
        begin
        Selected.Position.Y :=Selected.Position.Y - STEP;
        exit;
        end;
  end; //case

  //(((((((((((((((((((((((((((((   (((((((((((((((((  ((((((((((  (((((((
  //If execution flow reaches this point it means the atom take the right place
  //It is -correctly- positioned in its last movement avoiding approx error
  Selected.Position.X:=-7*CELL_LENGTH+CELL_LENGTH*Selected.FDestination.col;
  Selected.Position.Y:=(7*CELL_LENGTH) - CELL_LENGTH*Selected.FDestination.row;

  Selected.IsMoving:=FALSE;
  cd.Enabled:=FALSE; //...we don't want CPU 100%

  if not IsCompleted then
    Applymover;
end;

//------------------------------------------------------------------------------
procedure TPlayField.SetLevel(const Value: string);
var
  Line:string;
  i,j:integer;
  ps,wl: TGLBaseSceneObject;      //Added object
  po:TGLProxyObject;
  tempAtom:TWAtom;
  firstAtom:TWAtom;

begin
  ClearField;
  FLevel := Value;
 

  if not FileExists(FLevel) then
  begin
    MessageDlg('Level file not found'+
      #13#10+
      'Check your ''levels'' dir path !',
      mtError,[mbOk],0);
    Exit;
  end;

  LevelFile:=TMemIniFile.Create(value);
  Molek_Name:=LevelFile.ReadString('Level','Name',' ');

  wl:=nil;
  ps:=nil;
  FirstAtom:=nil;//pointer to first atom created

  for i := 0 to 14 do
  begin

    if i<10 then
      Line:=LevelFile.ReadString('Level','feld_'+ IntToHex((i),2),'NaS')
    else
      Line:=LevelFile.ReadString('Level','feld_'+ IntToStr(i),'NaS');
    //raise if NaS

    for  j:= 0 to 14 do
    begin

      case Line[j+1] of
        '0'..'z':                             //----------------------------ATOM
        begin

          //---here is an implementation of
          //a 'on the fly' circular linked list
          //so that every atom has a pointer to the next
          //and the last one refers to the first one.

          tempAtom:=TWatom(ps); //ps initially nil
          ps:= TWatom(Objects.AddNewChild(TWAtom)); //Create atom
          //TWAtom(ps).PF:=Self;

          if tempAtom <> nil then
            tempAtom.NextAtom:=TWAtom(ps)
          else  //first atom just created
            FirstAtom:=TWAtom(ps);
          //---

          with TWatom(ps) do
          begin
            AtomID:=Line[j+1];             //Set the Atom ID
            PlayGrid[j,i]:=Line[j+1];      //Refresh text grid   [0..x]

            //Now we set the chemical aspect of the atom
            //through a string formatted as follow:
            //<Chemical>-[<bind1><bind2>...<bindn>]
            Spec:=LevelFile.ReadString('Level','atom_'+ Line[j+1],'NaS');

            Row:=i;
            Col:=j;
            Position.X:=-CELL_LENGTH*7 {- CELL_LENGTH/2}+CELL_LENGTH*(j);
            Position.Y:=CELL_LENGTH*7 {+ CELL_LENGTH/2}-CELL_LENGTH*(i);
            Position.Z:=0;

            Visible:=TRUE;
          end;
        end;                           //-------------------------------ATOM-END

        '#':                           //-----------------------------------WALL
        begin
          if (wl=nil) then  //If this is the first wall rendered...
          begin
            wl:= TGLSprite(Objects.AddNewChild(TGLSprite));
            TGLSprite(wl).Material.Texture:=TGLTexture(FWallBitmap);
            TGLSprite(wl).Material.Texture.Disabled:=FALSE;

            //PlayGrid[j,i]:=Line[j+1];     //Refresh Array Grid  (#)
            PlayGrid[j,i]:='#';
            TGLSprite(wl).Position.X:=-CELL_LENGTH*7 {- CELL_LENGTH/2}+CELL_LENGTH*(j);
            TGLSprite(wl).Position.Y:=CELL_LENGTH*7 {+ CELL_LENGTH/2}-CELL_LENGTH*(i);
            TGLSprite(wl).Position.Z:=0;

            TGLSprite(wl).Height:=CELL_LENGTH;
            TGLSprite(wl).Width:=CELL_LENGTH;

            TGLSprite(wl).Visible:=TRUE;

            TGLSprite(wl).Position.z:=0;
          end
          else
          begin//else       // if not first wall then proxies the first one
            po:=TGLProxyObject(Objects.AddNewChild(TGLProxyObject));
            po.MasterObject:=TGlSprite(wl);
            po.ProxyOptions:=[pooObjects];

            //PlayGrid[j,i]:=Line[j+1];     //Refresh Array Grid  (#)}
            PlayGrid[j,i]:='#';
            po.Position.X:=-CELL_LENGTH*7 {- CELL_LENGTH/2}+CELL_LENGTH*(j);
            po.Position.Y:=CELL_LENGTH*7 {+ CELL_LENGTH/2}-CELL_LENGTH*(i);
            po.Position.Z:=0;

            po.Visible:=TRUE;

            po.Position.z:=0;
          end;//else end
        end;//wall end                  //------------------------------WALL-END

      else //(.)                        //----------------------------------VOID
        PlayGrid[j,i]:=Line[j+1];      //Refresh Array Grid (.)
      end;//case
    end;//for2
  end;//for1


  //closed linked list;
  //[LastAtom].nextatom = FirstAtom
  TWatom(ps).NextAtom:=FirstAtom;

  i:=0;
  j:=0;
  Line:=LevelFile.ReadString('Level','mole_'+ IntToStr(i),'NaS');

  if GoalViewer<>nil then
  begin
    while Line<>'NaS' do
    begin
      while j<=(Length(Line)-1) do
      begin
        if ( (Line[j+1] in ['0'..'z']) ) then
        begin
          ps:= TWatom(GoalViewer.Objects.AddNewChild(TWAtom));

          TWAtom(ps).Height:=V_CELL_LENGTH;
          TWAtom(ps).Width:=V_CELL_LENGTH;
          with TWatom(ps) do
          begin
            AtomID:=Line[j+1];  //Set Atom ID
            // Send identificative string to  Atom
            Spec:=LevelFile.ReadString('Level','atom_'+ Line[j+1],'NaS');

            Position.X:=-V_CELL_LENGTH*7{- V_CELL_LENGTH/2}+V_CELL_LENGTH*(j);
            Position.Y:=V_CELL_LENGTH*7{+ V_CELL_LENGTH/2}-V_CELL_LENGTH*(i);
            Position.Z:=0;
            Visible:=TRUE;
          end;
        end;
        j:=j+1;
      end;
      j:=0;
      i:=i+1;
      Line:=LevelFile.ReadString('Level','mole_'+ IntToStr(i),'NaS');
    end;
  end;

  if not LevelFile.SectionExists('SAVED') then
    begin
    //Standard level file, Level number has to be extracted
    //from filename;
    FLevelNumber:=StrToInt(Copy(ExtractFileName(FLevel),7,3)) ;
    FMoves:=0;
    end
  else
    //Custom saved level file: we don't have a "standard filename"
    //so we extract the number of level from an early saved
    //field.
    begin
    FLevelNumber:=LevelFile.ReadInteger('SAVED','levelnumber',1);
    FMoves:= LevelFile.ReadInteger('SAVED','moves',0);
    end;
end;


//------------------------------------------------------------------------------
//Warning: all the MoveAtom... procedures work correctly ONLY in Short-Circuit
//Boolean evaluation. If no SCB, then 'Range check error' will rise.
//----
procedure TPlayField.MoveAtomLeft;  //See MoveAtomRight for English translation
var
  tomove:integer;//contiene l'indice della colonna di arrivo

begin
  if (not(Assigned(Selected))) then exit;

  if Selected.IsMoving then Exit;

  tomove:=Selected.Col;//il movimento parte dalla Col. selezionata, verso sx

  repeat
    tomove:=tomove-1; //sposta di una casella a sx...
  until
    (tomove=-1) or
    not(PlayGrid[tomove,Selected.Row] = '.');
     //fino al muro o atomo

  tomove:=tomove+1; //quando esce dal ciclo ha gi� letto una casella bloccata

  if tomove=Selected.Col then exit; //Nessuna possibilit� di movim.

  PlayGrid[Selected.Col,Selected.Row]:='.' ;  //la pos. di partenza si svuota...
  PlayGrid[tomove,Selected.Row]:=(Selected.AtomID[1]);//quella di arrivo viene
                                                      //riempita dall'ID

  Selected.Col:=tomove; //cambio l'attributo Col dell'atomo
  Selected.FDestination.row:=Selected.FRow;
  Selected.FDestination.col:=tomove;//indico la casella di arrivo  per CAD...
  Selected.MovType:=mLeft;//... e gli dico che movimento attuare...
  Selected.IsMoving:=TRUE;
  RemoveMover;
  Moves:=Moves+1;
  cd.Enabled:=TRUE;
end;

//------------------------------------------------------------------------------
procedure TPlayField.MoveAtomDown;
var
  tomove:integer;

begin
  if (not(Assigned(Selected))) then exit;

  if Selected.IsMoving then Exit;

  tomove:=Selected.Row;

  repeat
    tomove:=tomove+1;
  until
    (tomove=15) or
    not(PlayGrid[Selected.col,tomove] = '.');
    //There is the possibility for no border wall

  tomove:=tomove-1; //quando esce dal ciclo ha gi� letto

  if tomove=Selected.Row then exit; //Nessuna possibilit� di movim.

  PlayGrid[Selected.Col,Selected.Row]:='.' ;
  PlayGrid[Selected.Col,tomove]:=(Selected.AtomID[1]);

  Selected.row:=tomove;
  Selected.FDestination.row:=tomove;
  Selected.FDestination.col:=Selected.FCol;
  Selected.MovType:=mDown;
  Selected.IsMoving:=TRUE;
  RemoveMover;
  Moves:=Moves+1;
  cd.Enabled:=TRUE;
end;

//------------------------------------------------------------------------------
procedure TPlayField.MoveAtomRight;
var
  tomove:integer;//Destination column index

begin
  if (not(Assigned(Selected))) then exit; //Check for selection

  if Selected.IsMoving then Exit;  //Check for running atom

  tomove:=Selected.Col;

  //Increase the column index until
  //there is possibility for movement
  repeat
    tomove:=tomove+1;
  until
    (tomove=15) or
    not(PlayGrid[tomove,Selected.Row] = '.');

  tomove:=tomove-1 ;  //wall/Atom already readed: count back

  if tomove=Selected.Col then exit; //No movement possibility

  //Start Text grid position empty
  PlayGrid[Selected.Col,Selected.Row]:='.' ;

  //Final Text grid position setted with selected ATOmId
  PlayGrid[tomove,Selected.Row]:=(Selected.AtomID[1]);

  Selected.Col:=tomove;//Refresh Atom-column-index
  Selected.FDestination.row:=Selected.FRow;
  Selected.FDestination.col:=tomove;
  Selected.MovType:=mRight;
  Selected.IsMoving:=TRUE;
  RemoveMover;   //Remove movement arrows from ready-to-go atom
  Moves:=Moves+1;
  cd.Enabled:=TRUE;  //Start the cadencer
end;


//------------------------------------------------------------------------------
procedure TPlayField.MoveAtomUp;
var
  tomove:integer;

begin
  if (not(Assigned(Selected))) then exit;

  if Selected.IsMoving then Exit;

  tomove:=Selected.Row;

  repeat
    tomove:=tomove-1;
  until
    (tomove=-1) or
    not(PlayGrid[Selected.col,tomove] = '.');
    

  tomove:=tomove+1 ;

  if tomove=Selected.Row then exit; //Nessuna possibilit� di movim.

  PlayGrid[Selected.Col,Selected.Row]:='.' ;
  PlayGrid[Selected.col,tomove]:=(Selected.AtomID[1]);

  Selected.Row:=tomove;
  Selected.FDestination.row:=tomove;
  Selected.FDestination.col:=Selected.FCol;
  Selected.MovType:=mUp;
  Selected.IsMoving:=TRUE;
  RemoveMover;
  Moves:=Moves+1;
  cd.Enabled:=TRUE;
end;

//------------------------------------------------------------------------------
//Check for completed level: TRUE if completed, FALSE otherwise
function TPlayField.IsCompleted: boolean;
var
  goal_lines: array[0..14] of string;
  hL,vL,i,j,im,jm:integer;
  c:char;

begin
  for i:=0 to 14 do
    goal_lines[i]:='';

  //Trying to explain...
  //We create a sub-matrix of chars
  //(dimensions hL x vL)
  //containing the target-molecule...


  i:=0;
  repeat
    goal_lines[i]:=LevelFile.ReadString('Level','mole_'+IntToStr(i),'NaS');
    i:=i+1;
  until
    (i>14) or (goal_lines[i-1]='NaS');

  //Vertical lenght of char matrix representing target molecular form
  //0-based
  vL:=i-1;

  //Horizontal lenght of char matrix representing target molecular form
  //1-based
  hL:=Length(goal_lines[0]);


  //Checking for same length in all strings (thank you Mehdi);
  //if not the same then extend with '.'
  //Required for subsequent victory check
  for i := 1 to vL - 1 do
    if Length(goal_lines[i]) > hL then
      hL := Length(goal_lines[i]);
  for i := 0 to vL - 1 do
    if Length(goal_lines[i]) < hL then
      goal_lines[i] := goal_lines[i] + StringOfChar('.', hL - Length(goal_lines[i]));

  result:=FALSE;

  //...afterthat we move this
  //submatrix around the text-grid
  //looking for match.
  //While matching we have to
  //ignore the presence of
  //intermediate wall
  //so we use an extra temp var 'c'
  //Matching is done
  //line by line on the submatrix
  //and when there is no
  // match Result is false setted.

  for i := 0 to 15-vL do
  begin
    for j := 0 to 15-hL do
    begin
      result := TRUE;
      for  im:= 0  to vL-1 do
        begin
        for  jm:= 1 to hL do
          begin
          c:=PlayGrid[j+jm-1,i+im];
          //A simple consideration:
            //The min x-value of playgrid
            //is j+jm-1 => 0+1-1 = 0 OK !
            //The max, for j=15-hl and
            // jm = hl, is 15-hl+hl-1 = 14 OK !
            //y-min = 0+0 = 0 Ok!
            //y-max = 15-vL+vL-1 = 14 Ok!
          if c='#' then c:='.';
          if c <> goal_lines[im][jm] then
            result:=FALSE
          end;
        end;
      if result=TRUE then
        begin
        DoLevelCompleted; //Fire event
        exit;
        end;
    end;
  end;

end;

//------------------------------------------------------------------------------
procedure TPlayField.SetMolek_Name(const Value: string);
begin
  FMolek_Name := Value;
end;

//------------------------------------------------------------------------------
//Clear the graphic scene and the textual representative grid
//Frees the ini level file

procedure TPlayField.ClearField;
var
i,j,c:integer;

begin
  if (Selected<>nil) then
    if (Selected.IsMoving) then
        begin
        Cd.Enabled:=FALSE;
        Selected.IsMoving:=FALSE;
        RemoveMover;
        end;
  //Selected is nil (NOT TO BE FREED. IT's a TRUE pointer)
  //We have to nullify it BEFORE the full clearing
  //below because Selected can have a mover that
  //requires an existing atom on wich operate
  //...see (a-note) in setSelected method;
  Selected:=nil;

  //We have to use inverse for loop because we
  //are freeing list objects; so every time
  //an object is cleared List count decreases
  //by one and this behavior could cause
  //AV in step-forward order.

  c:=Objects.Count-1;

  for i := c downto 0 do
    begin
    if  (Objects.Children[i] is TGLDummyCube)  or
        (Objects.Children[i] is TGLLightSource)   then
        continue;

    Objects.Children[i].Free;
    end;

  //Free the goal viewer
  c:=GoalViewer.Objects.Count-1;

  for i := c downto 0 do
    begin
    if  (GoalViewer.Objects.Children[i] is TGLDummyCube)  or
        (GoalViewer.Objects.Children[i] is TGLLightSource)   then
        continue;

    GoalViewer.Objects.Children[i].Free;
    end;

  //Clean the text grid
  for  i:= 0 to 14 do
    for j := 0 to 14 do
    begin
      PlayGrid[i,j]:='.';
    end;

  //Free Ini level file
  FreeAndNil(LevelFile);
  moves:=0;
end;

//------------------------------------------------------------------------------
//Set the selected atom on the graphical scene applying
//movement arrows on it

procedure TPlayField.SetSelected(const Value: TWatom);
begin
  //(a-note)
  if ( (FSelected<>nil) and (FSelected.IsMoving) ) then exit;
  if FSelected<>nil then RemoveMover;
     FSelected := Value;

  if FSelected<>nil then ApplyMover;
end;

//------------------------------------------------------------------------------
//Applies the movement arrows to the selected atom

procedure TPlayField.ApplyMover;
var
  mv:TGLSprite;
  mv_bmp:TBitmap;

  //Check array range of Textual grid
  //Thanks to Mehdi
  function Check(a, b: Integer): Boolean;
  begin
    Result := True;
    if (a < 0) or (a > 14) or (b < 0) or (b > 14) then
      Result := False;
  end;

begin
  //mover bitmap init
  mv_bmp:=TBitmap.Create;
  mv_bmp.Height:=32;
  mv_bmp.Width:=32;
  mv_bmp.Transparent:=FALSE;

  //Compile with Boolean Short Ev. !!
  if Check(Selected.Col,Selected.Row-1) and
    (PlayGrid[Selected.Col,Selected.Row-1]='.')
    then
    begin
    mv:=TGLSprite(Selected.AddNewChild(TGLSprite));
    mv.Height:=CELL_LENGTH;
    mv.Width:=CELL_LENGTH;
    mv.Position.X:= 0;
    mv.Position.Y:= CELL_LENGTH;
    mv.Position.Z:=0;
    mv_bmp.LoadFromResourceName(hInstance,'F_S');
    mv.Material.Texture.Assign(mv_bmp);
    mv.Material.Texture.Disabled:=FALSE;
    mv.Tag:=1;
    end;

  if Check(Selected.Col-1,Selected.Row) and
  (PlayGrid[Selected.Col-1,Selected.Row]='.')
  then
    begin
    mv:=TGLSprite(Selected.AddNewChild(TGLSprite));

    mv.Height:=CELL_LENGTH;
    mv.Width:=CELL_LENGTH;

    mv.Position.X:= -CELL_LENGTH;
    mv.Position.Y:= 0;

    mv.Position.Z:=0;
    mv_bmp.LoadFromResourceName(hInstance,'F_SX');
    mv.Material.Texture.Assign(mv_bmp);
    mv.Material.Texture.Disabled:=FALSE;
    mv.Tag:=2;
    end;

  if Check(Selected.Col+1,Selected.Row) and
    (PlayGrid[Selected.Col+1,Selected.Row]='.') then
    begin
    mv:=TGLSprite(Selected.AddNewChild(TGLSprite));

    mv.Height:=CELL_LENGTH;
    mv.Width:=CELL_LENGTH;

    mv.Position.X:=CELL_LENGTH;
    mv.Position.Y:= 0 ;

    mv.Position.Z:=0;
    mv_bmp.LoadFromResourceName(hInstance,'F_DX');
    mv.Material.Texture.Assign(mv_bmp);
    mv.Material.Texture.Disabled:=FALSE;
    mv.Tag:=3;
    end;

  if Check(Selected.Col,Selected.Row+1) and
    (PlayGrid[Selected.Col,Selected.Row+1]='.') then
    begin
    mv:=TGLSprite(Selected.AddNewChild(TGLSprite));

    mv.Height:=CELL_LENGTH;
    mv.Width:=CELL_LENGTH;

    mv.Position.X:=0;
    mv.Position.Y:= -CELL_LENGTH;
    mv.Position.Z:=0;
    mv_bmp.LoadFromResourceName(hInstance,'F_G');
    mv.Material.Texture.Assign(mv_bmp);
    mv.Material.Texture.Disabled:=FALSE;
    mv.Tag:=4;
    end;

  mv_bmp.Free;
end;

//------------------------------------------------------------------------------
//Remove arrows from selected atom
procedure TPlayField.RemoveMover;
var
  i:integer;
begin
  if Selected = nil then exit;

  //Arrows are the only atom's children
  for i:=Selected.Count-1 downto 0 do
    Selected.Children[i].Free;
end;

//------------------------------------------------------------------------------
procedure TPlayField.SetMoves(const Value: integer);
begin
  FMoves := Value;
end;

//------------------------------------------------------------------------------
procedure TPlayField.SetOnLevelCompleted(const Value: TNotifyEvent);
begin
  FOnLevelCompleted := Value;
end;

//------------------------------------------------------------------------------
procedure TPlayField.DoLevelCompleted;
begin

  //delegated
  if Assigned(FOnLevelCompleted) then
    FOnLevelCompleted(Self);
end;

//------------------------------------------------------------------------------
//Save the current play in order to continue level later
//It creates a new ini file similar to standard level ones
//copying in it the current PlayGrid

procedure TPlayField.SaveLevel(AFileName:string);
var
  TempIni:TMeminiFile;
  Lst,TempLst:TStringList;
  Line:string;
  i,j:integer;

begin
  TempIni:=TMemIniFile.Create(AFileName);

  Lst:=TStringList.Create;
  Levelfile.GetStrings(Lst);
  //Lst contains levelfile strings

  TempLst:=TStringList.Create;

  //manually adding sections to TempLst
  TempLst.Add('[LEVEL]');
  Line:= LevelFile.ReadString('Level','Name','NaS');

  TempLst.Add('Name='+Line);

  //copying atom_* entries

  for i := 0 to Lst.Count-1 do
    begin
    if pos('atom',Lst[i])<>0 then
      TempLst.Add(Lst[i]);
    end;

  //storing in feld_* entries
  //the current grid

  for i := 0 to 14 do
    begin
    Line:='';

    for j:=0 to 14 do
      Line:=Line+PlayGrid[j,i];

    if i<10 then
      TempLst.Add('feld_'+ IntToHex((i),2)+'='+Line)
    else
      TempLst.Add('feld_'+ IntToStr(i)+'='+Line);
    end;

  Line:='';

  //copying mole_* entries
  for i := 0 to Lst.Count-1 do
    begin
    if pos('mole',Lst[i])<>0 then
      TempLst.Add(Lst[i]);
    end;


  //Adding a [saved] section
  //in order to recreate
  //play environment:
  //only user saved levels
  //have this section

  TempLst.Add('[SAVED]');
  TempLst.Add('moves'+'='+IntToStr(Moves));
  //TempLst.Add('levelnumber'+'='+Copy(ExtractFileName(FLevel),7,3));
  TempLst.Add('levelnumber'+'='+IntToStr(FLevelNumber));

  //Store entries on disk file
  TempIni.SetStrings(TempLst);

  //Write file
  TempIni.UpdateFile;

  TempLst.Free;
  Lst.Free;
  TempIni.Free;
end;

//---------------------------------------------------------------------------
//Load a previously saved level
procedure TPlayField.LoadLevel(AFileName:string);
begin
  Level:=AFileName;
  Moves:=LevelFile.ReadInteger('SAVED','moves',0);
end;

//-----------------------------------------------------------------------------

{ TWAtom }

constructor TWAtom.Create(AOWner: TComponent);
begin
  inherited;
  Height:=CELL_LENGTH;
  Width:=CELL_LENGTH;
  Direction.X:=0;
  Direction.Y:=0;
  Direction.Z:=1;
end;

//------------------------------------------------------------------------------
destructor TWAtom.Destroy;
begin
  DeleteChildren;
  inherited;
end;

//------------------------------------------------------------------------------
procedure TWAtom.SetAtomID(const Value: string);
begin
  FAtomId:=Value;
end;

//------------------------------------------------------------------------------
procedure TWAtom.SetCol(const Value: integer);
begin
  FCol := Value;
end;


//------------------------------------------------------------------------------
procedure TWAtom.SetIsMoving(const Value: boolean);
begin
  FIsMoving := Value;
end;

//------------------------------------------------------------------------------
procedure TWAtom.SetMovType(const Value: TMovement);
begin
  FMovType := Value;
end;

//------------------------------------------------------------------------------
procedure TWAtom.SetNextAtom(const Value: TWAtom);
begin
  FNextAtom := Value;
end;

//------------------------------------------------------------------------------
procedure TWAtom.SetRow(const Value: integer);
begin
  FRow := Value;
end;

//----------------------------------------------------------------------------
//fissa le caratteristiche dell'atomo in funione della stringa identificativa
//(En.)Applies chem and binds to the atom using ID string (see above)

{Here I had a !!!!BIG!!!! problem;
Until version 1.2.2.x, after sliding for a while the level selector,
the exception EOutOfResources was raised by the app. Why ???
I've found the answer in the following stuff...
As stated in Borland(c) quality central reports, TBitmap implementation in
Graphics.pas unit produces massive GDI leakages for two principal reasons:

1 (qc n. 21695):

  "The TBitmap.LoadFromResourceName and
  TBitmap.LoadFromResourceID produce a GDI-handle leak because
  the device context of the BitmapCanvas is not released.
  In TBitmap.ReadDIB the FHandle field is overwritten by the new value.
  So in all methods except the both above there is a call to FreeContext
  before ReadDIB is called (see TBitmap.ReadStream).
  But in LoadFromResourceName and LoadFromResourceID this FreeContext-call
  is missing.

  Sometimes this leak is not visible because FreeMemoryContexts is called in
  TWinControl.MainWndProc. So if you do not load a resource bitmap in a
  short interval the FreeMemoryContexts will clean up the handle that would
  leak if TBitmap would create a new internal Windows's bitmap.

  But this leak will be high visible if you minimize the application
  or move the mouse cursor to another
  process because then MainWndProc is never or infrequently called
  and all further LoadFromResourceXx calls for the same bitmap will increase
  the GDI-handle usage.
  This bug is also in Delphi 7 "

  >> As a workaround I've inserted (as suggested by the author of report)
  TBitmap.Assign(nil) calls to force the FreeContext call  before any
  call to TBitmap.LoadFromResourceName

2 (qc n. 13540)

  "Using TBitmap to load and paint images very often can result in massive gdi
  leaks. This is because TBitmapImage tries to delete a palette which is still
  selected in an memory context.

  The bug is also in the Delphi IDE (2005). If you open many units so that
  the tabset doesnt fit anymore then you will get two button.
  Open the taskmanager and display the gdi handle column.
  Than move other windows over this buttons. Watch the handle count."

  >> This problem has not workaround until Delphi 2006 or an update from Borland.

  However, after some experiments with MemProof(c) it seems that only
  first issue had severe impact on WAtomic so I think that problem is possibly
  solved.

}

procedure TWAtom.SetSpec(const Value: string);
var
  _Chem:char;
  _Bind:string; //Chem contiene la specie chimica, _Bind il/i legame/i
  i:integer;
  _B:char;


begin
  FSpec:=Value;
  //Value contains informations about atom type and binds
  //as the following schema:
  //<Chemical>-[<bind1><bind2>...<bindn>]
  _Chem:=Value[1];
  _Bind:=Copy(Value,3,8); //Copia in _bind il tipo di legame

  temp_atom.Transparent:=FALSE;

  temp.Assign(nil); //**see the note above -> forces FreeContext call
  temp.LoadFromResourceName(hinstance,'BLACK');
  temp.Transparent:=TRUE;

  //Black is the transparent color
  Binds_Graph.Assign(nil);
  Binds_graph.LoadFromResourceName(hinstance,'BLACK');
  Binds_graph.Transparent:=TRUE;
  Binds_graph.TransparentMode:=tmFixed;
  Binds_Graph.TransparentColor:=Binds_Graph.Canvas.Pixels[0,0];
  temp.TransparentColor := Binds_Graph.TransparentColor;

  temp_atom.Assign(nil);

  //Specie CHIMICA
  case _Chem of
    '1':
      temp_atom.LoadFromResourceName(hinstance,'IDROGENO');

    '2':
      temp_atom.LoadFromResourceName(hinstance,'CARBONIO');

    '3':
      temp_atom.LoadFromResourceName(hinstance,'OSSIGENO');

    '4':
      temp_atom.LoadFromResourceName(hinstance,'AZOTO');

    '5':
      temp_atom.LoadFromResourceName(hinstance,'ZOLFO');

    '6':
      temp_atom.LoadFromResourceName(hinstance,'FLUORO');

    '7':
      temp_atom.LoadFromResourceName(hinstance,'CLORO');

    '8':
      temp_atom.LoadFromResourceName(hinstance,'BROMO');

    '9':
      temp_atom.LoadFromResourceName(hinstance,'FOSFORO');

    'o':
      temp_atom.LoadFromResourceName(hinstance,'CRISTAL');

    'A':
      temp_atom.LoadFromResourceName(hinstance,'L_H');

    'B':
      temp_atom.LoadFromResourceName(hinstance,'L_SWNE');

    'C':
      temp_atom.LoadFromResourceName(hinstance,'L_V');

    //-- from here... atoms spec are NO MORE compatible
    //   qwith KAtomic so be careful with back-port
    'D':
      temp_atom.LoadFromResourceName(hinstance,'UNO');

    'E':
      temp_atom.LoadFromResourceName(hinstance,'DUE');

    'F':
      temp_atom.LoadFromResourceName(hinstance,'TRE');

    'G':
      temp_atom.LoadFromResourceName(hinstance,'QUATTRO');

    'H':
      temp_atom.LoadFromResourceName(hinstance,'CINQUE');

    'I':
      temp_atom.LoadFromResourceName(hinstance,'SEI');

    'L':
      temp_atom.LoadFromResourceName(hinstance,'SETTE');

    'M':
      temp_atom.LoadFromResourceName(hinstance,'OTTO');

    'N':
      temp_atom.LoadFromResourceName(hinstance,'NOVE');

    'O':
      temp_atom.LoadFromResourceName(hinstance,'DIECI');

    'P':
      temp_atom.LoadFromResourceName(hinstance,'UNDICI');

    'Q':
      temp_atom.LoadFromResourceName(hinstance,'DODICI');

    'R':
      temp_atom.LoadFromResourceName(hinstance,'TREDICI');

    'S':
      temp_atom.LoadFromResourceName(hinstance,'QUATTORDICI');

    'T':
      temp_atom.LoadFromResourceName(hinstance,'QUINDICI');
     //-- ..to here.
      end;

  i:=1;
  while i<=length(_Bind) do
    begin
    _B:=_Bind[i];
    temp.Assign(nil); //forces FreeContext call... see above
    case _B of
    'c':
      begin
      temp.LoadFromResourceName(hinstance,'S_E');
      end;

    'g':
      begin
      temp.LoadFromResourceName(hinstance,'S_W');
      end;

     'a':
      begin
      temp.LoadFromResourceName(hinstance,'S_N');
      end;

      'e':
      begin
      temp.LoadFromResourceName(hinstance,'S_S');
      //temp.TransparentColor:=temp.Canvas.Pixels[0,31];
      end;

      'd':
      begin
      temp.LoadFromResourceName(hinstance,'S_SE');
      end;

      'b':
      begin
      temp.LoadFromResourceName(hinstance,'S_NE');
      end;

      'h':
      begin
      temp.LoadFromResourceName(hinstance,'S_NW');
      //temp.TransparentColor:=temp.Canvas.Pixels[31,31];
      end;

      'f':
      begin
      temp.LoadFromResourceName(hinstance,'S_SW');
      end;

      'A':
      begin
      temp.LoadFromResourceName(hinstance,'D_N');
      end;

      'C':
      begin
      temp.LoadFromResourceName(hinstance,'D_S');
      end;

      'D':
      begin
      temp.LoadFromResourceName(hinstance,'D_W');
      end;

      'B':
      begin
      temp.LoadFromResourceName(hinstance,'D_E');
      end;

      'F':
      begin
      temp.LoadFromResourceName(hinstance,'T_E');
      end;

      'H':
      begin
      temp.LoadFromResourceName(hinstance,'T_W');
      end;

      'G':
      begin
      temp.LoadFromResourceName(hinstance,'T_S');
      end;

      'E':
      begin
      temp.LoadFromResourceName(hinstance,'T_N');
      end;

    end;

    temp.Transparent:=TRUE;

    //temp_atom.Canvas.CopyMode:=cmSrcCopy;
    //Binds_graph.Canvas.Draw(0,0,temp);
    Binds_graph.Canvas.StretchDraw(r,temp);
    i:=i+1;
  end;

  //temp_atom.Canvas.CopyMode:=cmSrcCopy;
  //temp_atom.Canvas.Draw(0,0,Binds_graph);
  temp_atom.Canvas.StretchDraw(r,Binds_graph);

  material.Texture.Disabled:=TRUE;
  material.Texture.TextureMode:=tmDecal;
  material.Texture:=TGLTexture(temp_atom);
  material.Texture.TextureFormat:=tfDefault;

  //----------------------------------------------------------------------------
  material.Texture.TextureWrap:=twNone;

  //IF THIS PRODUCES A "NOT VALID ENUMERATOR" ERROR
  //LOOK AT LINE 3996 GLTexture.pas and try to change the following
  //" if GL_VERSION_1_2 or GL_EXT_texture_edge_clamp then begin //Original "
  //...with...
  //" if not GL_VERSION_1_2 and not GL_EXT_texture_edge_clamp then begin "//Fede
  //However this is a workaround: the problem is possibly located
  //in display drivers ( try to update )
  //You can also use another workaround: place in WAtomic directory
  //the mesa3D dll that you can find at GLScene site; it will provide
  //a full OpenGL software emulation ( slow but possibly OK ! )
  //Last try ... comment out the line :)
  //----------------------------------------------------------------------------


  //material.Texture.Compression:=tcHighSpeed;
  material.Texture.Disabled:=FALSE;

  //material.Texture.MagFilter:= maNearest;
  //material.Texture.MinFilter:= miNearest; //prevents txtr distortion in gv
  //material.Texture.MinFilter:= miLinear;
  //material.Texture.MinFilter:=miLinearMipMapLinear;
  //material.Texture.MinFilter:=miLinearMipMapNearest;
  //material.Texture.MinFilter:=miNearestMipMapLinear; //this also works...
  //material.Texture.MinFilter:=miNearestMipMapNearest;

  Visible:=TRUE;

  //FreeAndNil(temp);
  //FreeAndNil(Binds_graph);
  //FreeAndNil(Temp_atom);

end;
//---------------------------------------------------------------------------

initialization
  //Initialize global variables

  //Create Bitmap objects to use in WAtom.SetSpec
  temp:=TBitmap.Create;
  Binds_graph:=TBitmap.Create;
  temp_atom:=TBitmap.Create;
  Binds_graph.PixelFormat:=pf8bit;
  temp_atom.PixelFormat:=pf8bit;
  temp.PixelFormat:=pf8bit;

  //Bitmap sizes are power of 2
  temp.Height:=32;
  temp.Width:=32;
  temp_atom.Height:=32;
  temp_atom.Width:=32;
  Binds_graph.Height:=32;
  Binds_graph.width:=32;

  r.Left:=0;
  r.Top:=0;
  r.Bottom:=31;
  r.Right:=31;

  //0,0.......0,31
  //..............
  //..............
  //31,0.....31,31

finalization

  FreeAndNil(temp);
  FreeAndNil(Binds_graph);
  FreeAndNil(Temp_atom);


  
//--------------------------------
// ironfede@users.sourceforge.net
//--------------------------------

end.
