unit opt;
{
------------------------------------------------------------------
WATOMIC - Windows(c) KAtomic clone

Copyright (C) 2004  Federico Blaseotto (ironfede)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
------------------------------------------------------------------
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, ComCtrls,IniFiles, ExtCtrls;

type
  TOptForm = class(TForm)
    Edit1: TEdit;
    OkBtn: TButton;
    Label1: TLabel;
    TrackBar1: TTrackBar;
    WSizeRgp: TRadioGroup;
    Panel1: TPanel;
    FrameSkCb: TCheckBox;
    BrowserRgp: TRadioGroup;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure TrackBar1Change(Sender: TObject);
  private
    { Private declarations }
    InF:TIniFile;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses sh_fun, main;

//-----------------------------------------------------------------------------
//On settings window activation load saved values
procedure TOptForm.FormActivate(Sender: TObject);
begin
  //Speed
  edit1.Text:=IntToStr(Inf.ReadInteger('SETTINGS','speed',5));
  TrackBar1.Position:=StrToInt(edit1.Text);

  //Prevent frame skipping
  FrameSkCb.Checked:=(Inf.ReadBool('SETTINGS','preventfrskip',FALSE));

  //Size
  WSizeRgp.ItemIndex:=Inf.ReadInteger('SETTINGS','wsize',1);

  //Default browser for info pages
  BrowserRgP.ItemIndex:=inf.ReadInteger('SETTINGS','browser',0);

end;

//-----------------------------------------------------------------------------
//On settings window CREATION open the INI File prepared by
//sh_fun.InitEnv
procedure TOptForm.FormCreate(Sender: TObject);
begin
  InF:=TIniFile.Create(MAIN_INI_FILENAME);
end;

procedure TOptForm.FormDestroy(Sender: TObject);
begin
  Inf.UpdateFile;
  Inf.Free;
end;

//-----------------------------------------------------------------------------
//On settings window close Writes Values
//sh_fun.InitEnv
procedure TOptForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Inf.WriteInteger('SETTINGS','speed',StrToInt(edit1.Text));
  Inf.WriteInteger('SETTINGS','wsize',WSizeRgp.ItemIndex);

  if WSizeRgp.ItemIndex=2 then
  begin
    Inf.WriteInteger('SETTINGS','width',MainForm.Width);
    Inf.WriteInteger('SETTINGS','height',MainForm.Height);
  end;

  //Prevent frame skipping
  Inf.WriteBool('SETTINGS','preventfrskip',FrameSkCb.Checked);

  //Default browser for info pages
  if BrowserRgP.ItemIndex=0 then
    inf.WriteInteger('SETTINGS','browser',0) //iexplore
  else
    inf.WriteInteger('SETTINGS','browser',1); //firefox

  Inf.UpdateFile;
end;

procedure TOptForm.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in ['0'..'9']) then Key:=#0;
end;

procedure TOptForm.TrackBar1Change(Sender: TObject);
begin
  edit1.Text:=IntToStr(TrackBar1.Position);
end;



//---------------------------------------------------------------------------

//--------------------------------
// ironfede@users.sourceforge.net
//--------------------------------

end.
