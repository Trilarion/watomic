object hsfrm: Thsfrm
  Left = 312
  Top = 179
  BorderStyle = bsDialog
  Caption = 'High Scores'
  ClientHeight = 367
  ClientWidth = 411
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 411
    Height = 367
    Align = alClient
    TabOrder = 0
    object ListView1: TListView
      Left = 1
      Top = 1
      Width = 409
      Height = 304
      Align = alTop
      Color = clNone
      Columns = <
        item
          AutoSize = True
          Caption = 'Player'
          MinWidth = 80
        end
        item
          AutoSize = True
          Caption = 'Score'
          MinWidth = 60
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Franklin Gothic Book'
      Font.Style = []
      ReadOnly = True
      ParentFont = False
      TabOrder = 0
      ViewStyle = vsReport
    end
    object OkBtn: TButton
      Left = 8
      Top = 320
      Width = 393
      Height = 25
      Cancel = True
      Caption = 'Close'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Franklin Gothic Book'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 1
    end
  end
end
