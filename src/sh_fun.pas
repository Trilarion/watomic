unit sh_fun;
{
------------------------------------------------------------------
WATOMIC - Windows(c) KAtomic clone

Copyright (C) 2005  Federico Blaseotto (ironfede)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
------------------------------------------------------------------
}

interface
  //This function prepares the execution environment
  //of WAtomic storing ini filenames and getting the
  //correct Application Directory path in order
  //to allow WAtomic running in multiuser mode (e.g w2k-ntfs)
  function InitEnv:boolean;

var
  HS_INI_FILENAME:string;
  MAIN_INI_FILENAME:string;

implementation

uses shellAPI,shlObj,ActiveX,Windows,Forms,SysUtils;

//------------------------------------------------------------------------------
//It's a caller job cleaning the allocator
procedure FreePidl(pidl: PItemIDList);
var
  allocator: IMalloc;
  
begin
  if Succeeded(SHGetMalloc(allocator)) then
  begin
    allocator.Free(pidl);
{$IFDEF VER90}
    allocator.Release;
{$ENDIF}
  end;
end;

//------------------------------------------------------------------------------
//Get the application data directory to write high scores
function GetAppDataPath(var sPath: string): Boolean;
var
  pidl: PItemIDList;
  buf: array[0..MAX_PATH] of char;

begin
  result := false;

  if Succeeded(ShGetSpecialFolderLocation(
    Application.Handle,
    CSIDL_APPDATA,
    pidl
    )) then
  begin
    if ShGetPathfromIDList(pidl, buf) then
    begin
      result := true;
      sPath := string(buf);
    end;
    FreePidl(pidl);
  end;
end;


//------------------------------------------------------------------------------
function InitEnv:boolean;
var
  AppPath:string;
begin
  try
    if not GetAppDataPath(AppPath) then
    begin
      Result:=FALSE;
      Exit;
    end;

    if not DirectoryExists(AppPath+'\WAtomic') then
      Mkdir(AppPath+'\WAtomic');

    MAIN_INI_FILENAME :=AppPath + '\WAtomic\WAtomic.ini';
    HS_INI_FILENAME :=AppPath + '\WAtomic\hs.ini';

    Result:=TRUE;
  except
    Result:=FALSE;
  end;
end;

//--------------------------------
// ironfede@users.sourceforge.net
//--------------------------------

end.
