unit AtomSelect;
{
------------------------------------------------------------------
WATOMIC Level Editor- Level editor for WAtomic

Copyright (C) 2005  Mehdi Farhadi

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
------------------------------------------------------------------
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Math;

type
  TBinding = (btNone, btSingle, btDouble, btTriple);

  TBindings = array[1..8] of TBinding;

  TAtomSelectForm = class(TForm)
    OkButton: TButton;
    CancelButton: TButton;
    AtomComboBox: TComboBox;
    BindingsGroupBox: TGroupBox;
    AtomImage: TImage;
    AtomLabel: TLabel;
    NECheckBox: TCheckBox;
    SECheckBox: TCheckBox;
    SWCheckBox: TCheckBox;
    NWCheckBox: TCheckBox;
    NCheckBox: TCheckBox;
    ECheckBox: TCheckBox;
    SCheckBox: TCheckBox;
    WCheckBox: TCheckBox;
    NComboBox: TComboBox;
    EComboBox: TComboBox;
    SComboBox: TComboBox;
    WComboBox: TComboBox;
    Bevel1: TBevel;
    procedure FormCreate(Sender: TObject);
    procedure AtomComboBoxChange(Sender: TObject);
    procedure NCheckBoxClick(Sender: TObject);
  private
    function GetBindings: TBindings;
    procedure SetBindings(Value: TBindings);
    function GetSpec: String;
    procedure SetSpec(Value: String);
    procedure RefreshAtomImage;

    property Bindings: TBindings read GetBindings write SetBindings;
  public
    property Spec: String read GetSpec write SetSpec;
  end;

var
  AtomSelectForm: TAtomSelectForm;

implementation

{$R *.dfm}

{ TAtomSelectForm }

const
  AtomResNames: array[0..11] of String =
    ('IDROGENO', 'CARBONIO', 'OSSIGENO',
     'AZOTO','ZOLFO','FLUORO','CLORO','FOSFORO',
     'CRISTAL', 'L_H', 'L_V', 'L_SWNE');

  AtomChems: array [0..11] of Char =
    ('1', '2', '3', '4', '5', '6', '7', '9', 'o', 'A', 'C', 'D');

  AtomNames: array[0..11] of String =
    ('Hydrogen', 'Carbon', 'Oxygen', 'Azote',
     'Sulphur','Fluor','Chlorine','Phosphorus', 'Crystal',
     'Line (Horizontal)', 'Line (Vertical)', 'Line (Diagonal)');

  BindingNames: array[1..8, btSingle..btTriple] of String =
    (('a', 'A', 'E'), ('b', '', ''),
     ('c', 'B', 'F'), ('d', '', ''),
     ('e', 'C', 'G'), ('f', '', ''),
     ('g', 'D', 'H'), ('h', '', ''));

  BindingResNames: array[1..8, btSingle..btTriple] of String =
    (('S_N', 'D_N', 'T_N'), ('S_NE', '', ''),
     ('S_E', 'D_E', 'T_E'), ('S_SE', '', ''),
     ('S_S', 'D_S', 'T_S'), ('S_SW', '', ''),
     ('S_W', 'D_W', 'T_W'), ('S_NW', '', ''));

procedure TAtomSelectForm.FormCreate(Sender: TObject);
var
  I: Integer;
  EmptyBindings: TBindings;
begin
  for I := 0 to Length(AtomNames) - 1 do
    AtomComboBox.Items.Add(AtomNames[I]);
  AtomComboBox.ItemIndex := 0;
  for I := 1 to 8 do
    EmptyBindings[I] := btNone;
  Bindings := EmptyBindings;
  RefreshAtomImage;
end;

function TAtomSelectForm.GetSpec: String;
var
  AtomBindings: TBindings;
  BindingsString: String;
  I: Integer;
begin
  Result := AtomChems[AtomComboBox.ItemIndex];
  AtomBindings := Bindings;
  BindingsString := '';
  for I := 1 to 8 do
    if AtomBindings[I] <> btNone then
      if BindingResNames[I, AtomBindings[I]] <> '' then
        BindingsString := BindingsString + BindingNames[I, AtomBindings[I]];
  if BindingsString <> '' then
    Result := Result + '-' + BindingsString;
end;

procedure TAtomSelectForm.SetSpec(Value: String);
var
  Chem: Char;
  I, J: Integer;
  K: TBinding;
  ChemIndex: Integer;
  AtomBindings: TBindings;
begin
  if Value = '' then
    Exit;
  Chem := Value[1];
  ChemIndex := -1;
  for I := 0 to Length(AtomChems) - 1 do
    if AtomChems[I] = Chem then
      ChemIndex := I;
  if ChemIndex = -1 then
    Exit;
  AtomComboBox.ItemIndex := ChemIndex;
  for I := 1 to 8 do
    AtomBindings[I] := btNone;
  for I := 3 to Length(Value) do
    for J := 1 to 8 do
      for K := btSingle to btTriple do
        if BindingNames[J, K] = Value[I] then
          AtomBindings[J] := K;
  Bindings := AtomBindings;
  RefreshAtomImage;
end;

procedure TAtomSelectForm.RefreshAtomImage;
var
  AtomBindings: TBindings;
  I: Integer;
  Bitmap: TBitmap;
  BackColor: TColor;
begin
  AtomImage.Picture.Bitmap.LoadFromResourceName(HInstance, 'BLACK');
  BackColor := AtomImage.Picture.Bitmap.Canvas.Pixels[0, 0];
  AtomImage.Picture.Bitmap.LoadFromResourceName(HInstance, AtomResNames[AtomComboBox.ItemIndex]);
  AtomBindings := Bindings;
  Bitmap := TBitmap.Create;
  try
    Bitmap.TransparentMode := tmFixed;
    Bitmap.TransparentColor := BackColor;
    Bitmap.Transparent := True;
    for I := 1 to 8 do
    begin
      if AtomBindings[I] = btNone then
        Continue;
      Bitmap.LoadFromResourceName(HInstance, BindingResNames[I, AtomBindings[I]]);
      AtomImage.Canvas.Draw(0, 0, Bitmap);
    end;
  finally
    Bitmap.Free;
  end;
end;

procedure TAtomSelectForm.AtomComboBoxChange(Sender: TObject);
begin
  RefreshAtomImage;
end;

function TAtomSelectForm.GetBindings: TBindings;
var
  I: Integer;
begin
  for I := 1 to 8 do
    Result[I] := btNone;

  if NECheckBox.Checked then
    Result[2] := btSingle;
  if SECheckBox.Checked then
    Result[4] := btSingle;
  if SWCheckBox.Checked then
    Result[6] := btSingle;
  if NWCheckBox.Checked then
    Result[8] := btSingle;
    
  if NCheckBox.Checked then
    Result[1] := TBinding(NComboBox.ItemIndex + 1);
  if ECheckBox.Checked then
    Result[3] := TBinding(EComboBox.ItemIndex + 1);
  if SCheckBox.Checked then
    Result[5] := TBinding(SComboBox.ItemIndex + 1);
  if WCheckBox.Checked then
    Result[7] := TBinding(WComboBox.ItemIndex + 1);
end;

procedure TAtomSelectForm.SetBindings(Value: TBindings);
begin
  NECheckBox.Checked := Value[2] <> btNone;
  SECheckBox.Checked := Value[4] <> btNone;
  SWCheckBox.Checked := Value[6] <> btNone;
  NWCheckBox.Checked := Value[8] <> btNone;

  NCheckBox.Checked := Value[1] <> btNone;
  ECheckBox.Checked := Value[3] <> btNone;
  SCheckBox.Checked := Value[5] <> btNone;
  WCheckBox.Checked := Value[7] <> btNone;

  NComboBox.Enabled := NCheckBox.Checked;
  EComboBox.Enabled := ECheckBox.Checked;
  SComboBox.Enabled := SCheckBox.Checked;
  WComboBox.Enabled := WCheckBox.Checked;

  NComboBox.ItemIndex := Max(Ord(Value[1]) - 1, 0);
  EComboBox.ItemIndex := Max(Ord(Value[3]) - 1, 0);
  SComboBox.ItemIndex := Max(Ord(Value[5]) - 1, 0);
  WComboBox.ItemIndex := Max(Ord(Value[7]) - 1, 0);
end;

procedure TAtomSelectForm.NCheckBoxClick(Sender: TObject);
begin
  NComboBox.Enabled := NCheckBox.Checked;
  EComboBox.Enabled := ECheckBox.Checked;
  SComboBox.Enabled := SCheckBox.Checked;
  WComboBox.Enabled := WCheckBox.Checked;
  RefreshAtomImage;
end;

end.
