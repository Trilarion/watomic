program WAtomicLevelEditor;

uses
  Forms,
  editormain in 'editormain.pas' {MainForm},
  about in 'about.pas' {AboutForm},
  field in 'field.pas',
  sh_fun in 'sh_fun.pas',
  AtomSelect in 'AtomSelect.pas' {AtomSelectForm};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'WAtomic Level Editor';
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
