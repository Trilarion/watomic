object AtomSelectForm: TAtomSelectForm
  Left = 192
  Top = 114
  BorderStyle = bsDialog
  Caption = 'Atom'
  ClientHeight = 239
  ClientWidth = 294
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object AtomImage: TImage
    Left = 240
    Top = 10
    Width = 32
    Height = 32
  end
  object AtomLabel: TLabel
    Left = 24
    Top = 20
    Width = 27
    Height = 13
    Caption = 'Atom:'
  end
  object OkButton: TButton
    Left = 120
    Top = 208
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object CancelButton: TButton
    Left = 208
    Top = 208
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object AtomComboBox: TComboBox
    Left = 64
    Top = 16
    Width = 153
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = AtomComboBoxChange
  end
  object BindingsGroupBox: TGroupBox
    Left = 8
    Top = 48
    Width = 278
    Height = 153
    Caption = 'Bindings'
    TabOrder = 1
    object Bevel1: TBevel
      Left = 96
      Top = 16
      Width = 8
      Height = 129
      Shape = bsLeftLine
      Style = bsRaised
    end
    object NECheckBox: TCheckBox
      Left = 8
      Top = 24
      Width = 88
      Height = 17
      Caption = 'Up-Right'
      TabOrder = 0
      OnClick = AtomComboBoxChange
    end
    object SECheckBox: TCheckBox
      Left = 8
      Top = 56
      Width = 88
      Height = 17
      Caption = 'Bottom-Right'
      TabOrder = 1
      OnClick = AtomComboBoxChange
    end
    object SWCheckBox: TCheckBox
      Left = 8
      Top = 88
      Width = 88
      Height = 17
      Caption = 'Bottom-Left'
      TabOrder = 2
      OnClick = AtomComboBoxChange
    end
    object NWCheckBox: TCheckBox
      Left = 8
      Top = 120
      Width = 88
      Height = 17
      Caption = 'Up-Left'
      TabOrder = 3
      OnClick = AtomComboBoxChange
    end
    object NCheckBox: TCheckBox
      Left = 112
      Top = 24
      Width = 70
      Height = 17
      Caption = 'Up'
      TabOrder = 4
      OnClick = NCheckBoxClick
    end
    object ECheckBox: TCheckBox
      Left = 112
      Top = 56
      Width = 70
      Height = 17
      Caption = 'Right'
      TabOrder = 6
      OnClick = NCheckBoxClick
    end
    object SCheckBox: TCheckBox
      Left = 112
      Top = 88
      Width = 70
      Height = 17
      Caption = 'Bottom'
      TabOrder = 8
      OnClick = NCheckBoxClick
    end
    object WCheckBox: TCheckBox
      Left = 112
      Top = 120
      Width = 70
      Height = 17
      Caption = 'Left'
      TabOrder = 10
      OnClick = NCheckBoxClick
    end
    object NComboBox: TComboBox
      Left = 184
      Top = 22
      Width = 81
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 5
      Text = 'Single'
      OnChange = AtomComboBoxChange
      Items.Strings = (
        'Single'
        'Double'
        'Triple')
    end
    object EComboBox: TComboBox
      Left = 184
      Top = 54
      Width = 81
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 7
      Text = 'Single'
      OnChange = AtomComboBoxChange
      Items.Strings = (
        'Single'
        'Double'
        'Triple')
    end
    object SComboBox: TComboBox
      Left = 184
      Top = 86
      Width = 81
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 9
      Text = 'Single'
      OnChange = AtomComboBoxChange
      Items.Strings = (
        'Single'
        'Double'
        'Triple')
    end
    object WComboBox: TComboBox
      Left = 184
      Top = 118
      Width = 81
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 11
      Text = 'Single'
      OnChange = AtomComboBoxChange
      Items.Strings = (
        'Single'
        'Double'
        'Triple')
    end
  end
end
