This is a beta version of WAtomic editor.
Despite its "beta" status, it's fully functional... it only needs to be tested and enanched in some points.

The original code has been provided to me by Mehdi Farhadi.
The original field.pas unit was slightly modified by Mehdi Farhadi to create a compatible shared unit between WAtomic and WAtomic editor.

Images have to be compiled in a resource file before the project build.
You will need GLScene framework to compile WAtomic editor.

Please give me feedback on this project:

ironfede@users.sourceforge.net

For further information see WAtomic original release.

TODO:
-Code documentation;
-Adding chemical species;
-Better integration with WAtomic;